﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Models
{
    public class Indentor
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IndentorId { get; set; }
        public string Name { get; set; }
        public string Landlline { get; set; }

        public string Extension { get; set; }

        public string Mobile { get; set; }

        public int DivisionId { get; set; }
        public virtual Division Division { get; set; }

        public int DesignationId { get; set; }
        public virtual Designation Designation { get; set; }


    }
}
