﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Models
{
   public class AppRole :IdentityRole
    {
        [Required, StringLength(100)]
        public DateTime Created { get; set; }

        [Required, StringLength(500)]
        public string Description { get; set; }
    }
}
