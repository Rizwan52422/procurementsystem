﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Models
{
   public class MajorType
    {
        public MajorType()
        {
            Categories = new HashSet<Category>();
        }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MajorTypeId { get; set; }

        [Required]
        [StringLength(4)]
        public string Code { get; set; }

        [Required]
        [StringLength(50)]
        public string Description { get; set; }

        public virtual ICollection<Category> Categories { get; set; }
    }
}
