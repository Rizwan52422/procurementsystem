﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
   public class Supplier
    {
        public Supplier()
        {
            PurchaseOrders = new HashSet<PurchaseOrder>();
        }
        public int SupplierId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Landline { get; set; }
        public string Mobile { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string AccountNo { get; set; }

        public virtual ICollection<PurchaseOrder> PurchaseOrders { get; set; }
    }
}
