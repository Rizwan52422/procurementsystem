﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Models
{
  public  class Item
    {
        public Item()
        {

        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ItemId { get; set; }

        [Required]
        public int SubCategoryId { get; set; }
        public virtual SubCategory SubCategory { get; set; }

        [Required]
        public int BrandId { get; set; }
        public virtual Brand Brand { get; set; }

        [Required]
        public int ColorId { get; set; }
        public virtual Color Color { get; set; }

        [Required]
        public string Capacity { get; set; }

        public virtual ICollection<PurchaseOrderItem> PurchaseOrderItems { get; set; }

    }
}
