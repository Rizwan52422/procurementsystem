﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Models
{
    public class PurchaseOrderItem
    {
        public PurchaseOrderItem()
        {

        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PurchaseOrderItemId { get; set; }

        [Required]
        public int ItemId { get; set; }
        public virtual Item Item { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BatchNumber { get; set; }

        [Required]
        public int Quantity { get; set; }

        [Required]
        public decimal UnitRate { get; set; }

        [Required]
        public decimal Paid { get; set; }


        [StringLength(15)]
        public string BillNumber { get; set; }

        [StringLength(25)]
        public string ChequeNumber { get; set; }

        public DateTime? ChequeDate { get; set; }

        public decimal? Amount { get; set; }

        [StringLength(150)]
        public string Bank { get; set; }

        [Required]
        public bool IsWarranty { get; set; }
        public int? WarrantyInMonths { get; set; }


        [Required]
        public int PurchaseOrderId { get; set; }
        public virtual PurchaseOrder PurchaseOrder { get; set; }


        [Required]
        public int PaymentTypeId { get; set; }
        public virtual PaymentType PaymentType { get; set; }

        [Required]
        public int UnitId { get; set; }
        public virtual Unit Unit { get; set; }

        public int Descount { get; set; }
        public int Total { get; set; }


    }
}
