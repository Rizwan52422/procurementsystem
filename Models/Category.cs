﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Models
{
   public class Category
    {
        public Category()
        {
            SubCategories = new HashSet<SubCategory>();
        }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CategoryId { get; set; }

        [Required]
        [StringLength(4)]
        public string Code { get; set; }

        [Required]
        [StringLength(50)]
        public string Description { get; set; }

        [Required]
        public int MajorTypeId { get; set; }
        public virtual MajorType MajorType { get; set; }

        public virtual ICollection<SubCategory> SubCategories { get; set; }
    }
}
