﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Models
{
   public class Division
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DivisionId { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public int ProjectId { get; set; }
        public virtual Project Project { get; set; }
    }
}
