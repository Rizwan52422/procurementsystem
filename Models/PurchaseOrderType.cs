﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Models
{
   public class PurchaseOrderType
    {
        public PurchaseOrderType()
        {
            PurchaseOrders = new HashSet<PurchaseOrder>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PurchaseOrderTypeId { get; set; }

        [Required]
        [StringLength(50)]
        public string Description { get; set; }

        public virtual ICollection<PurchaseOrder> PurchaseOrders { get; set; }
    }
}
