﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
   public class Project
    {
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string Code { get; set; }
    }
}
