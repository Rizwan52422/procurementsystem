﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Models
{
   public class Designation
    {
        public Designation()
        {
            Indentors = new HashSet<Indentor>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DesignationId { get; set; }

        public string Name { get; set; }

        public string Short { get; set; }

        public virtual ICollection<Indentor> Indentors { get; set; }
    }
}
