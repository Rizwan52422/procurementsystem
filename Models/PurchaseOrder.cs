﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Models
{
   public class PurchaseOrder
    {

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PurchaseOrderId { get; set; }
        [Required]
        public string PurchaseOrderNumber { get; set; }
        [Required]
        public DateTime PurchaseOrderDate { get; set; }

        [Required]
        public int SupplierId { get; set; }
        public virtual Supplier Supplier { get; set; }

        [Required]
        public int PurchaseOrderTypeId { get; set; }
        public virtual PurchaseOrderType PurchaseOrderType { get; set; }

        [Required]
        [StringLength(25)]
        public string InvoiceNumber { get; set; }

        [Required]
        public DateTime InvoiceDate { get; set; }

        public virtual ICollection<PurchaseOrderItem> PurchaseOrderItems { get; set; }
    }
}
