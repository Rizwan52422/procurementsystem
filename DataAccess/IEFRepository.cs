﻿using Models;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace DataAccess
{
    public interface IEFRepository
    {
        void Add(object entity);
        void Delete(object entity);
        bool SaveChanges();
        void Update(object entity);

        IQueryable<Project> GetProjects();

        Project GetProject(int Id);
    }
}