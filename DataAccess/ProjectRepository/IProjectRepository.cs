﻿using System.Collections.Generic;
using Models;

namespace DataAccess.ProjectRepository
{
    public interface IProjectRepository
    {
        IEnumerable<Project> Getprojects();
    }
}