﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccess.ProjectRepository
{
    public class ProjectRepository : IProjectRepository
    {
        private readonly ProcurementDbContext _context;

        public ProjectRepository(ProcurementDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Project> Getprojects()
        {
           return _context.Projects.ToList();
        }
    }
}
