﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Models;

namespace DataAccess
{
    public class EFRepository : IEFRepository
    {
        private readonly ProcurementDbContext _context;

        public EFRepository(ProcurementDbContext context)
        {
            _context = context;
        }

        #region Generic Operations
        public void Add(object entity)
        {
            _context.Add(entity);
        }
        public void Update(object entity)
        {
            _context.Update(entity);
        }
        public void Delete(object entity)
        {
            _context.Remove(entity);
        }
        public bool SaveChanges()
        {
            return _context.SaveChanges() > 0;
        }


        public IQueryable<Project> GetProjects()
        {

            return _context.Projects;
        }

        public Project GetProject(int Id)
        {
            return _context.Projects
                           .FirstOrDefault(p => p.ProjectId == Id);
        }

        #endregion

    }
}
