﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccess
{
    public class ProcurementDbContext :IdentityDbContext<AppUser,AppRole,string>
    {
        private readonly IConfigurationRoot _config;

        public ProcurementDbContext(DbContextOptions<ProcurementDbContext> options,IConfigurationRoot config)
            : base(options)
        {
            _config = config;
        }
        public DbSet<Designation> Designations { get; set; }
        public DbSet<Division>  Divisions { get; set; }
        public DbSet<Indentor> Indentors{ get; set; }
        public DbSet<Project>  Projects { get; set; }



        public DbSet<Color>  Colors { get; set; }
        public DbSet<Category>  Categories { get; set; }
        public DbSet<SubCategory>  SubCategories { get; set; }
        public DbSet<Brand>  Brands { get; set; }
        public DbSet<MajorType>  MajorTypes { get; set; }
        public DbSet<PaymentType>  PaymentTypes { get; set; }
        public DbSet<Item>  Items { get; set; }
        public DbSet<Unit>  Units { get; set; }
        public DbSet<Supplier>  Suppliers { get; set; }
        //purchase tables below
        public DbSet<PurchaseOrder>  PurchaseOrders { get; set; }
        public DbSet<PurchaseOrderItem>  PurchaseOrderItems { get; set; }
        public DbSet<PurchaseOrderType>  PurchaseOrderTypes { get; set; }

        

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_config["ConnectionStrings:ProcurementDbConnection"]);
        }

    }
}
