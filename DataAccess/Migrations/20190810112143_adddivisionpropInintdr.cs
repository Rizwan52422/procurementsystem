﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccess.Migrations
{
    public partial class adddivisionpropInintdr : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Indentors_Designations_DesignationId",
                table: "Indentors");

            migrationBuilder.AlterColumn<int>(
                name: "DesignationId",
                table: "Indentors",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DivisionId",
                table: "Indentors",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Indentors_DivisionId",
                table: "Indentors",
                column: "DivisionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Indentors_Designations_DesignationId",
                table: "Indentors",
                column: "DesignationId",
                principalTable: "Designations",
                principalColumn: "DesignationId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Indentors_Divisions_DivisionId",
                table: "Indentors",
                column: "DivisionId",
                principalTable: "Divisions",
                principalColumn: "DivisionId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Indentors_Designations_DesignationId",
                table: "Indentors");

            migrationBuilder.DropForeignKey(
                name: "FK_Indentors_Divisions_DivisionId",
                table: "Indentors");

            migrationBuilder.DropIndex(
                name: "IX_Indentors_DivisionId",
                table: "Indentors");

            migrationBuilder.DropColumn(
                name: "DivisionId",
                table: "Indentors");

            migrationBuilder.AlterColumn<int>(
                name: "DesignationId",
                table: "Indentors",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Indentors_Designations_DesignationId",
                table: "Indentors",
                column: "DesignationId",
                principalTable: "Designations",
                principalColumn: "DesignationId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
