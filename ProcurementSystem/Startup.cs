﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Models;

namespace ProcurementSystem
{
    public class Startup
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _config;

        public Startup(IHostingEnvironment env )
        {
            _env = env;
            var builder = new ConfigurationBuilder().SetBasePath(_env.ContentRootPath)
              .AddJsonFile("appsettings.json");

            _config = builder.Build();
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(_config);
            services.AddDbContext<ProcurementDbContext>();
            services.AddTransient<IEFRepository, EFRepository>();
            services.AddIdentity<AppUser, AppRole>()
                .AddEntityFrameworkStores<ProcurementDbContext>();

            //set cookies
            services.ConfigureApplicationCookie(config =>
            {
                config.AccessDeniedPath = "/Errors/AccessDenied";
                config.Cookie.HttpOnly = true;
                config.Cookie.MaxAge = TimeSpan.FromMinutes(60);
                config.LoginPath = "/Auth/SignIn";
                config.LogoutPath = "/Auth/SignOut";
                config.ReturnUrlParameter = "returnUrl";
                config.SlidingExpiration = true;

            });

            services.Configure<IdentityOptions>(option =>
            {
                //lockout Settings
                option.Lockout.MaxFailedAccessAttempts = 3;
                option.Lockout.AllowedForNewUsers = true;
                //Password Settings
                option.Password.RequireDigit = false;
                option.Password.RequiredLength = 4;
                option.Password.RequiredUniqueChars = 0;
                option.Password.RequireLowercase = false;
                option.Password.RequireNonAlphanumeric = false;
                option.Password.RequireUppercase = false;
                //SignIn Settings
                option.SignIn.RequireConfirmedEmail = false;
                option.SignIn.RequireConfirmedPhoneNumber = false;
                //OtherSettigns
                //option.Stores.ProtectPersonalData = true;
                option.User.RequireUniqueEmail = true;
            });

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app)
        {
            if (_env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseAuthentication();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=DashBoard}/{action=Index}/{id?}");
            });

        }
    }
}
