﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Models;

namespace UI.Controllers
{
    public class CustomValidationController : Controller
    {
        private UserManager<AppUser> _userManager;

        public CustomValidationController(UserManager<AppUser> userManager)
        {
            _userManager = userManager;
        }


        public IActionResult IsEmailAvailable(string Email)
        {
            var result = _userManager.Users
                                     .Any(p => string.Compare(p.Email, Email, true) == 0);
            
return Json(!result);
        }

        public IActionResult IsUsernameAvailable(string Username)
        {
            var result = _userManager.Users
                                     .Any(p => string.Compare(p.UserName, Username, true) == 0);

            return Json(!result);
        }
    }
}