﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Models;
using UI.Models;

namespace UI.Controllers
{
    public class AuthController : Controller
    {
        private SignInManager<AppUser> _signInManager;
        private UserManager<AppUser> _userManager;

        public AuthController(SignInManager<AppUser> signInManager,
            UserManager<AppUser> userManager)
        {
            _signInManager = signInManager;
            _userManager = userManager;

        }
        public IActionResult Index()
        {
            return View();
        }

        #region Sign In
        [HttpGet]
        public IActionResult SignIn()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SignIn(SignInViewModel model, string returnUrl = "")
        {
            if (ModelState.IsValid)
            {
                //TODO: Database validation code goes here

                var result = await _signInManager.PasswordSignInAsync(
                    userName: model.UserName,
                    password: model.Password,
                    isPersistent: model.RememberMe,
                    lockoutOnFailure: false);

                if (result.Succeeded)
                {
                    if (!string.IsNullOrEmpty(returnUrl) && Url.IsLocalUrl(returnUrl))
                        return Redirect(returnUrl);
                    else
                        return RedirectToAction("Index", new { controller = "Home" });
                }

                if (result.IsLockedOut || result.IsNotAllowed)
                {
                    ModelState.AddModelError("", "Your account is blocked. Contact application manager / support.");
                }
            }
            else
            {
                ModelState.AddModelError("", "Username / password is invalid");
            }

            return View(model);
        }
        #endregion

        #region Sign Up
        [HttpGet]
        public IActionResult SignUp()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SignUp(SignUpViewModel model)
        {
            if (ModelState.IsValid)
            {
                //TODO: Add your code here

                var user = new AppUser()
                {
                    Created = DateTime.Now,
                    DateOfBirth = model.DateOfBirth,
                    Email = model.Email,
                    Name = model.Name,
                    UserName = model.Username
                };

                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    return RedirectToAction("SignIn", new { controller = "Auth" });
                }
            }

            ModelState.AddModelError("", "Provide all required data to proceed");
            return View(model);
        }
        #endregion

        #region Sign Out
        public async Task<IActionResult> SignOut()
        {
            // SIGNOUT
            await _signInManager.SignOutAsync();
            return RedirectToAction("SignIn", new { controller = "Auth" });
        }
        #endregion
    }
}