﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace UI.Models
{
    public class SignInViewModel
    {
        [Display(Name = "Usrename")]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(25, ErrorMessage = "{0} must not exceeds {2} -{1}", MinimumLength = 4)]
        public string UserName { get; set; }

        [Display(Name = "Password")]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(25, ErrorMessage = "{0} must not exceeds {2} - {1} characters", MinimumLength = 4)]
        public string Password { get; set; }

        [Required]
        public bool RememberMe { get; set; }
    }
}
