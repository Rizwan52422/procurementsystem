﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace UI.Models
{
    public class ProjectViewModel
    {

        public int ProjectId { get; set; }

        [Required]
        [StringLength(50,ErrorMessage ="{0} must not exceed {1}")]
        public string ProjectName { get; set; }

        [Required]
        [StringLength(20,ErrorMessage ="Code must not exceed 20 Characters")]

        public string Code { get; set; }
    }
}
