﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace UI.Models
{
    public class SignUpViewModel
    {
        [Display(Name = "Name")]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(25, ErrorMessage = "{0} must not exceeds {2} - {1} characters", MinimumLength = 4)]
        public string Name { get; set; }

        [Display(Name = "Email")]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(150, ErrorMessage = "{0} must not exceeds {1} characters")]
        [Remote("IsEmailAvailable", "CustomValidation", null, ErrorMessage = "{0} already exists")]
        [EmailAddress]
        public string Email { get; set; }

        [Display(Name = "Date of birth")]
        [Required(ErrorMessage = "{0} is required")]
        public DateTime DateOfBirth { get; set; }

        [Display(Name = "Username")]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(25, ErrorMessage = "{0} must not exceeds {2} - {1} characters", MinimumLength = 4)]
        [Remote("IsUsernameAvailable", "CustomValidation", null, ErrorMessage = "{0} already exists")]
        public string Username { get; set; }

        [Display(Name = "Password")]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(25, ErrorMessage = "{0} must not exceeds {2} - {1} characters", MinimumLength = 4)]
        public string Password { get; set; }

        [Display(Name = "Confirm Password")]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(25, ErrorMessage = "{0} must not exceeds {2} - {1} characters", MinimumLength = 4)]
        [Compare("Password", ErrorMessage = "Please enter the same passwords")]
        public string ConfirmPassword { get; set; }
    }
}
