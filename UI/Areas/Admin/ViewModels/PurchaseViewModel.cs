﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UI.Areas.Admin.ViewModels
{
    public class PurchaseViewModel
    {

        public PurchaseOrder PurchaseOrder { get; set; }
        public List<PurchaseOrderItem> PurchaseOrderItems { get; set; }

        public Item Items { get; set; }

    }
}
