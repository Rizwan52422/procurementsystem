﻿using Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace UI.Areas.Admin.ViewModels
{
    public class ItemViewModel
    {
        public int ItemId { get; set; }

        [Required]
        public int SubCategoryId { get; set; }
        public virtual SubCategory SubCategory { get; set; }

        [Required]
        public int BrandId { get; set; }
        public virtual Brand Brand { get; set; }

        [Required]
        public int ColorId { get; set; }
        public virtual Color Color { get; set; }

        [Required]
        public string Capacity { get; set; }

    }
}
