﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess;
using Microsoft.AspNetCore.Mvc;
using UI.Models;
using Models;

namespace UI.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class ProjectController : Controller
    {
        private readonly IEFRepository _repository;

        public ProjectController(IEFRepository repository)
        {
            _repository = repository;
        }
       
        public IActionResult Index()
        {
            ViewBag.PageName = "Project List";
            
            return View();
        }

        [HttpGet]
        public IActionResult GetProjects()
        {
            return ViewComponent("UI.ViewComponents.ProjectList");
        }

        #region ProjectCreate
        [HttpGet]
        public IActionResult _Create()
        {
            return PartialView();
        }
        [HttpPost]
        public IActionResult _Create(ProjectViewModel model)
        {
            bool Status = false;
            string Message = string.Empty;
            if (ModelState.IsValid)
            {
                _repository.Add(new Project()
                {
                    Code = model.Code,
                    ProjectName = model.ProjectName
                });
                if (_repository.SaveChanges())
                {
                    Status = true;
                    Message = "Records Sucessfully Added.";
                }
                else
                {
                    Status = false;
                    Message = "Error creating record";
                }
            }
            else
            {
                ModelState.AddModelError("", "Provide all valid data to proceed");
            }
            return Json(new { status = Status, message = Message });
        }
        #endregion

        #region Project Edit
        [HttpGet]
        public IActionResult _Edit(int id)
        {
            var entry = _repository.GetProject(id);
            if (entry == null)
            {
                return NotFound();
            }

            var model = new ProjectViewModel()
            {
              ProjectId=entry.ProjectId,
               Code=entry.Code,
               ProjectName=entry.ProjectName
            };

            return PartialView(model);
        }

        [HttpPost]
        public ActionResult _Edit(ProjectViewModel model)
        {
            bool Status = false;
            string Message = string.Empty;
            if (ModelState.IsValid)
            {
                var result = _repository.GetProject(model.ProjectId);
                if (result != null)
                {
                    result.Code = model.Code;
                    result.ProjectName = model.ProjectName;
                    _repository.Update(result);
                    if (_repository.SaveChanges())
                    {
                        Status = true;
                        Message = "Record Update Successfully ";
                        ViewBag.Message = "Successfully Update Recourd";
                    }
                }
            }
            else
            {
                Message = "Error :Please Provide All Required Fields. ";
                ModelState.AddModelError("", "Error ! Please Provide All Required Fields.");
            }
            return Json(new { status = Status, message = Message });
        }
        #endregion

        #region Delete Project

        public ActionResult Delete(int Id)
        {
            bool Status = false;
            string Message = string.Empty;
            var project = _repository.GetProject(Id);
           _repository.Delete(project);
            _repository.Update(project);

            if (_repository.SaveChanges())
            {
                Status = true;
                Message = "Project Delete Successfully.";
            }
            return Json(new { status = Status, message = Message });
        }

        #endregion

    }
}