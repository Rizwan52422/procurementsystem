﻿using DataAccess.ProjectRepository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UI.Models;

namespace UI.ViewComponents
{
    public class ProjectListViewComponent : ViewComponent
    {
        private readonly IProjectRepository _projectRepository;

        public ProjectListViewComponent(IProjectRepository projectRepository)
        {
            _projectRepository = projectRepository;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var items = await GetItemsAsync();
            return View(items);
        }

        private Task<List<ProjectViewModel>> GetItemsAsync()
        {
            List<ProjectViewModel> model = null;
            var result = _projectRepository.Getprojects();
            if (result != null)
            {
                model = result.Select(p => new ProjectViewModel()
                {
                    ProjectId=p.ProjectId,
                    Code = p.Code,
                    ProjectName = p.ProjectName,
                   
                }).ToList();
            }

            return Task.FromResult(model);
        }
    }
}
